import { User } from "../GlobalTypes/globalTypes";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../Redux/reduxStore";

const checkIfUserIsRemembered = (): boolean => {
  return localStorage.getItem("userDto") !== null || sessionStorage.getItem("userDto") !== null;
};

const getUserDto = (): User => {
  return JSON.parse(localStorage.getItem("userDto") || sessionStorage.getItem("userDto") || "");
};

const removeUserDto = () => {
  localStorage.removeItem("userDto");
  sessionStorage.removeItem("userDto");
};

const initialUserState: userSliceState = {
  user: checkIfUserIsRemembered() ? getUserDto() : null
};

type userSliceState = {
  user: User | null;
}

const userSlice = createSlice({
  name: 'userReducer',
  initialState: initialUserState,
  reducers: {
    login: (state: userSliceState, action: PayloadAction<User>) => {
      state.user = action.payload;
    },
    logout: (state: userSliceState) => {
      state.user = null;
      removeUserDto();
    }
  }
});

export const { login, logout } = userSlice.actions
export const selectUser = (state: RootState) => state.userState.user;
export default userSlice.reducer;
