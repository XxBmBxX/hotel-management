const styles = {
  navigationMenuContainer: {
    display: "flex",
    flexDirection: "row",
    color: "white",
    backgroundColor: "#5099F4"
  },
  navigationButton: {
    margin: "0 5px",
    padding: "5px 25px",
    width: "fit-content",
    backgroundColor: "white",
    "&:hover": {
      cursor: "pointer",
      backgroundColor: "white"
    }
  },
  navigationButtonActive: {
    margin: "0 5px",
    padding: "5px 25px",
    width: "fit-content",
    backgroundColor: "#E8E8E8",
    "&:hover": {
      cursor: "pointer",
      backgroundColor: "white"
    }
  },
  authenticationContainer: {
    display: "flex",
    width: "fit-content",
    justifyContent: "space-evenly",
    alignItems: "center",
    color: "white",
    margin: "0 0 0 auto"
  },
  authenticationButton: {
    margin: "0 5px",
    color: "white",
    backgroundColor: "#F27800",
    "&:hover": {
      cursor: "pointer",
      backgroundColor: "#BD5E00"
    }
  }
};

export default styles;
