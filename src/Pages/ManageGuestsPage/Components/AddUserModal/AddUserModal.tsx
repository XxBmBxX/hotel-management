import { Box, Button, Modal, TextField, Typography } from "@mui/material";
import styles from "./addUserModalStyles";
import { validateTextField } from "../../../Utils/LoginRegisterHelpers/validateInputFields";
import React, { FunctionComponent, SetStateAction, useState } from "react";
import { GuestDto } from "../../../../GlobalTypes/globalTypes";
import useAlerts from "../../../../Components/Alert/useAlerts";
import SpringAlert from "../../../../Components/Alert/SpringAlert";
import { addGuestRequest } from "../../../../Axios/axiosRequests";
import useAuthentication from "../../../Utils/useAuthentication";
import CloseIcon from '@mui/icons-material/Close';

type AddUserModalProps = {
  fetchData: Function;
  open: boolean;
  setOpen: React.Dispatch<SetStateAction<boolean>>;
};

const AddUserModal: FunctionComponent<AddUserModalProps> = ({ fetchData, open, setOpen }) => {
  const guestDtoInitialState: GuestDto = {
    firstName: "",
    lastName: "",
    phoneNumber: ""
  };
  const [guestDto, setGuestDto] = useState<GuestDto>(guestDtoInitialState);
  const { userToken } = useAuthentication();
  const { alertText, isOpen, severity, showAlert } = useAlerts();

  const getFieldValue = (inputId: string): string => {
    return guestDto[inputId as keyof GuestDto];
  };

  const handleInput = (value: string, inputId: string) => {
    setGuestDto((currentState) => ({
      ...currentState,
      [inputId]: value
    }))
  };

  return (
    <Modal
      open={open}
      onClose={() => setOpen(!open)}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={styles.modalWrapper}>
        <Typography align={"center"} variant="h4" component="h2">
          Add guest
        </Typography>
        <TextField
          required
          fullWidth
          error={!validateTextField(getFieldValue("firstName"), { minCharacters: 2, maxCharacters: 20 }).isValid}
          helperText={validateTextField(getFieldValue("firstName"), { minCharacters: 2, maxCharacters: 20 }).helperText}
          value={guestDto.firstName}
          onChange={(event) => handleInput(event.target.value, "firstName")}
          margin={"dense"}
          label="First name"
          variant="filled"
        />
        <TextField
          required
          fullWidth
          error={!validateTextField(getFieldValue("lastName"), { minCharacters: 2, maxCharacters: 20 }).isValid}
          helperText={validateTextField(getFieldValue("lastName"), { minCharacters: 2, maxCharacters: 20 }).helperText}
          value={guestDto.lastName}
          onChange={(event) => handleInput(event.target.value, "lastName")}
          margin={"dense"}
          label="Last name"
          variant="filled"
        />
        <TextField
          sx={styles.marginInput}
          required
          fullWidth
          error={!validateTextField(getFieldValue("phoneNumber")).isValid}
          helperText={validateTextField(getFieldValue("phoneNumber")).helperText}
          value={guestDto.phoneNumber}
          onChange={(event) => handleInput(event.target.value, "phoneNumber")}
          margin={"dense"}
          label="Phone number"
          variant="filled"
        />
        <SpringAlert severity={severity} alertText={alertText} isOpen={isOpen} />
        <Button
          disabled={!validateTextField(getFieldValue("phoneNumber")).isValid
          || !validateTextField(getFieldValue("firstName")).isValid
          || !validateTextField(getFieldValue("lastName")).isValid}
          sx={styles.modalButton}
          onClick={() => addGuestRequest(guestDto, userToken, showAlert).then(() => fetchData())}
          variant="contained">
          Add guest
        </Button>
        <CloseIcon onClick={() => setOpen(!open)} sx={styles.closeButton} />
      </Box>
    </Modal>
  );
};

export default AddUserModal;
