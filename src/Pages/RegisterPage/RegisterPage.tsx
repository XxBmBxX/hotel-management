import styles from "../LoginPage/loginPageStyles";
import { Box, Button, TextField, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { RegisterFormProperties } from "../../GlobalTypes/loginRegisterTypes";
import { validateTextField } from "../Utils/LoginRegisterHelpers/validateInputFields";
import SpringAlert from "../../Components/Alert/SpringAlert";
import useAlerts from "../../Components/Alert/useAlerts";
import { registerRequest } from "../../Axios/axiosRequests";
import useAuthentication from "../Utils/useAuthentication";

const RegisterPage = () => {
  const navigate = useNavigate();
  const { isAuthenticated } = useAuthentication();

  if (isAuthenticated) {
    navigate("/rooms");
  }

  const registerFormInitialState: RegisterFormProperties = {
    firstName: "",
    lastName: "",
    password: "",
    username: ""
  };

  const [formProperties, setFormProperties] = useState<RegisterFormProperties>(registerFormInitialState);
  const { alertText, isOpen, severity, showAlert } = useAlerts();

  const getFieldValue = (inputId: string): string => {
    return formProperties[inputId as keyof RegisterFormProperties];
  };

  const checkAllFields = (): boolean => {
    return Object.keys(formProperties).some((property) => !validateTextField(getFieldValue(property)).isValid);
  };

  const handleInput = (value: string, inputId: string) => {
    setFormProperties((currentState) => ({
      ...currentState,
      [inputId]: value
    }));
  };

  return (
    <Box sx={styles.loginPageContentContainer}>
      <Box padding={6} paddingTop={2} paddingBottom={2} sx={styles.loginFormContainer}>
        <Typography marginBottom={2} color={"gray"} fontSize={18} align={"center"}>
          Make your hotelling easier!
        </Typography>
        <Typography fontSize={25} align={"center"}>
          Register
        </Typography>
        <TextField
          required
          fullWidth
          error={!validateTextField(getFieldValue("firstName")).isValid}
          helperText={validateTextField(getFieldValue("firstName")).helperText}
          value={formProperties.firstName}
          onChange={(event) => handleInput(event.target.value, "firstName")}
          margin={"dense"}
          label="First name"
          variant="filled"
        />
        <TextField
          required
          fullWidth
          error={!validateTextField(getFieldValue("lastName")).isValid}
          helperText={validateTextField(getFieldValue("lastName")).helperText}
          value={formProperties.lastName}
          onChange={(event) => handleInput(event.target.value, "lastName")}
          margin={"dense"}
          label="Last name"
          variant="filled"
        />
        <TextField
          required
          fullWidth
          error={!validateTextField(getFieldValue("username")).isValid}
          helperText={validateTextField(getFieldValue("username")).helperText}
          value={formProperties.username}
          onChange={(event) => handleInput(event.target.value, "username")}
          margin={"dense"}
          label="Username"
          variant="filled"
        />
        <TextField
          required
          fullWidth
          error={!validateTextField(getFieldValue("password")).isValid}
          helperText={validateTextField(getFieldValue("password")).helperText}
          value={formProperties.password}
          onChange={(event) => handleInput(event.target.value, "password")}
          margin={"dense"}
          label="Password"
          variant="filled"
          type={"password"}
        />
        <SpringAlert severity={severity} alertText={alertText} isOpen={isOpen} />
        <Button
          disabled={isOpen || checkAllFields()}
          onClick={() => registerRequest(formProperties, showAlert)}
          sx={styles.loginButton}
          variant="contained">
          Register
        </Button>
        <Button
          onClick={() => navigate("/login")}
          sx={styles.registrationTextButton}
          variant="text">
          Already have an account?
        </Button>
      </Box>
    </Box>
  );
};

export default RegisterPage;
