const styles = {
  tableContainer: {
    width: "80%",
    margin: "0 auto"
  },
  headerContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  headerButton: {
    height: "fit-content",
    marginLeft: 1
  },
  headerButtonsContainer: {
    marginLeft: "auto",
    marginRight: 1
  }
};

export default styles;
