import useAxios from "../../Axios/useAxios";
import { Room, RoomType } from "./Components/RoomCard/roomCardTypes";
import { Box } from "@mui/material";
import RoomSection from "./Components/RoomsSection/RoomSection";
import RoomsSideBar from "./Components/RoomsSideBar/RoomsSideBar";
import { useEffect, useState } from "react";
import { RoomsOverview } from "../../GlobalTypes/globalTypes";

const RoomsOverviewPage = () => {
  const initialRoomsState: RoomsOverview = {
    filteredRooms: [],
    rooms: []
  };

  const { data, fetchData } = useAxios<Room[]>("/rooms");
  const [roomsState, setRoomsState] = useState<RoomsOverview>(initialRoomsState);

  useEffect(() => {
    if (data !== null) {
      setRoomsState((currentState) => ({
        ...currentState,
        rooms: data,
        filteredRooms: data
      }));
    }
  }, [data]);

  const filterRooms = (filterOptions: Map<string, boolean>) => {
    let newFilteredRooms: Room[] = data !== null ? data : [];
    let roomTypesFiltered: Room[] = [];

    if (filterOptions.get("singleRooms")
      || filterOptions.get("doubleRooms")
      || filterOptions.get("familyRooms")) {
      if (filterOptions.get("singleRooms")) {
        roomTypesFiltered.push(...newFilteredRooms.filter((room) => room.roomType === RoomType.SINGLE ));
      }

      if (filterOptions.get("doubleRooms")) {
        roomTypesFiltered.push(...newFilteredRooms.filter((room) => room.roomType === RoomType.DOUBLE ));
      }

      if (filterOptions.get("familyRooms")) {
        roomTypesFiltered.push(...newFilteredRooms.filter((room) => room.roomType === RoomType.FAMILY ));
      }
    } else {
      roomTypesFiltered = newFilteredRooms;
    }

    if (filterOptions.get("freeRooms")) {
      roomTypesFiltered = roomTypesFiltered.filter((room) => room.guestGroup === null);
    }

    if (filterOptions.get("cleanedRooms")) {
      roomTypesFiltered = roomTypesFiltered.filter((room) => room.isClean);
    }

    if (newFilteredRooms !== null) {
      setRoomsState((currentState) => ({
        ...currentState,
        filteredRooms: roomTypesFiltered
      }));
    }
  };

  const getRoomsByType = (rooms: Room[], roomType: RoomType): Room[] => {
    return rooms.length > 0 ?
      rooms
      .filter((room) => room.roomType === roomType)
      .sort((room1, room2) => room1.roomNumber - room2.roomNumber)
      : [];
  };

  return (
    <Box>
      <RoomsSideBar rooms={roomsState.rooms} updateRooms={fetchData} filterRooms={filterRooms} />
      <Box>
        <RoomSection rooms={getRoomsByType(roomsState.filteredRooms, RoomType.SINGLE)} roomsType={"Single rooms"}/>
        <RoomSection rooms={getRoomsByType(roomsState.filteredRooms, RoomType.DOUBLE)} roomsType={"Double rooms"}/>
        <RoomSection rooms={getRoomsByType(roomsState.filteredRooms, RoomType.FAMILY)} roomsType={"Family rooms"}/>
      </Box>
    </Box>
  );
};

export default RoomsOverviewPage;
