import {GridColDef, GridValueGetterParams} from "@mui/x-data-grid";

const columns: GridColDef[] = [
  {
    field: "firstName",
    headerName: "First name",
    flex: 1,
    editable: true
  },
  {
    field: 'lastName',
    headerName: "Last name",
    flex: 1,
    editable: true
  },
  {
    field: 'phoneNumber',
    headerName: 'Number',
    sortable: false,
    flex: 1,
    editable: true
  },
  {
    field: 'fullName',
    headerName: 'Full name',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    flex: 1,
    editable: true,
    valueGetter: (params: GridValueGetterParams) =>
      `${params.row.firstName || ''} ${params.row.lastName || ''}`,
  },
];

export default columns;
