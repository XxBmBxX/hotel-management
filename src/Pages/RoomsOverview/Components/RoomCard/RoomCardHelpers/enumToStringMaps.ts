import { RoomStatus, RoomType } from "../roomCardTypes";

const roomStatusToStringMap = new Map<RoomStatus, string>([
  [RoomStatus.AVAILABLE, "available"],
  [RoomStatus.OCCUPIED, "occupied"],
  [RoomStatus.RESERVED, "reserved"]
]);

const roomTypeToIconNameMap = new Map<RoomType, string>([
  [RoomType.SINGLE, "person"],
  [RoomType.DOUBLE, "people"],
  [RoomType.FAMILY, "family_restroom"]
]);

const roomIsCleanToStringMap = new Map<boolean, string>([
  [true, "cleaned"],
  [false, "not cleaned"]
]);

export {
  roomStatusToStringMap,
  roomIsCleanToStringMap,
  roomTypeToIconNameMap
};
