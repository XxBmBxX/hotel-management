import { TextFieldConstraints, TextFieldValidity } from "../../../GlobalTypes/loginRegisterTypes";

const validateTextField = (textFieldValue: string, customConstraints?: TextFieldConstraints): TextFieldValidity => {
  let constraints: TextFieldConstraints;

  if (customConstraints === undefined) {
    constraints = {
      minCharacters: 5,
      maxCharacters: 30
    }
  } else {
    constraints = customConstraints
  }

  switch (true) {
    case (textFieldValue.length > constraints.maxCharacters):
      return { isValid: false, helperText: `This field allows maximum of ${constraints.maxCharacters} characters!`}
    case (textFieldValue.length < constraints.minCharacters):
      return { isValid: false, helperText: `This field requires more than ${constraints.minCharacters} characters!`}
    default: return { isValid: true, helperText: "" }
  }
};

export { validateTextField }
