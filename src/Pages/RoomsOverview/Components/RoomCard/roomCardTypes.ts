import { GuestGroup } from '../../../../GlobalTypes/globalTypes';

type Room = {
  id: number;
  roomNumber: number;
  roomType: RoomType;
  roomStatus: RoomStatus;
  isClean: boolean;
  reservedFrom: Date | null;
  reservedTo: Date | null;
  guestGroup: GuestGroup | null;
};

type RoomDto = {
  roomNumber: number;
  roomType: RoomType;
  roomStatus: RoomStatus;
  isClean: boolean;
};

type MakeReservationDto = {
  roomNumber: number;
  reservedFrom: Date | null;
  reservedTo: Date | null;
  guestGroup: GuestGroup;
  reservationPrice: number;
};

enum RoomType {
  SINGLE = "SINGLE",
  DOUBLE = "DOUBLE",
  FAMILY = "FAMILY"
}

enum RoomStatus {
  AVAILABLE = "AVAILABLE",
  OCCUPIED = "OCCUPIED",
  RESERVED = "RESERVED"
}

export type { Room, RoomDto, MakeReservationDto };
export { RoomType, RoomStatus };
