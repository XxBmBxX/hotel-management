import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../Redux/reduxStore";

const checkIfUserIsRemembered = (): boolean => {
  return localStorage.getItem("user-token") !== null || sessionStorage.getItem("user-token") !== null;
};

const getAuthenticationToken = (): string => {
  return localStorage.getItem("user-token") || sessionStorage.getItem("user-token") || "";
};

const removeAuthenticationToken = () => {
  localStorage.removeItem("user-token");
  sessionStorage.removeItem("user-token");
};

type IsAuthenticatedSliceState = {
  isAuthenticated: boolean;
  userToken: string;
};

const isAuthenticatedSliceInitialState: IsAuthenticatedSliceState = {
  isAuthenticated: checkIfUserIsRemembered(),
  userToken: getAuthenticationToken()
};

const isAuthenticatedSlice = createSlice({
  name: 'authenticationReducer',
  initialState: isAuthenticatedSliceInitialState,
  reducers: {
    setIsAuthenticated: (state: IsAuthenticatedSliceState) => {
      state.isAuthenticated = true;
      state.userToken = getAuthenticationToken();
    },
    setNotAuthenticated: (state: IsAuthenticatedSliceState) => {
      state.isAuthenticated = false;
      state.userToken = "";
      removeAuthenticationToken();
    }
  }
});

export const { setNotAuthenticated, setIsAuthenticated } = isAuthenticatedSlice.actions;
export const selectAuthentication = (state: RootState) => state.authenticationState.isAuthenticated;
export default isAuthenticatedSlice.reducer;
