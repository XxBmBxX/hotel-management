import { AlertSeverity } from "../../GlobalTypes/globalTypes";
import { useCallback, useState } from "react";

type ShowAlertFunction = {
  (alertText: string, severity: AlertSeverity): void;
}

type UseAlertsProperties = {
  severity: AlertSeverity;
  alertText: string;
  isOpen: boolean;
  showAlert: ShowAlertFunction;
}

const useAlerts = (): UseAlertsProperties => {

  const showAlert = useCallback((alertText: string, severity: AlertSeverity) => {
    setAlertProperties({
      alertText: alertText,
      isOpen: true,
      severity: severity,
      showAlert: showAlert
    });

    setTimeout(() => {
      setAlertProperties({
        alertText: "",
        isOpen: false,
        severity: severity,
        showAlert: showAlert
      });
    }, 5000)
  }, []);

  const initialProperties: UseAlertsProperties = {
    alertText: "",
    isOpen: false,
    severity: AlertSeverity.ERROR,
    showAlert: showAlert
  };
  const [alertProperties, setAlertProperties] = useState<UseAlertsProperties>(initialProperties);

  return alertProperties;
};

export default useAlerts;

export type { ShowAlertFunction }
