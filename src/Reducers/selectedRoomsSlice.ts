import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../Redux/reduxStore";

const initialSelectedRoomsState: SelectedRoomsSliceState = {
  roomIds: []
};

type SelectedRoomsSliceState = {
  roomIds: number[];
}

const selectedRoomsSlice = createSlice({
  name: 'selectedRoomsReducer',
  initialState: initialSelectedRoomsState,
  reducers: {
    addRoom: (state: SelectedRoomsSliceState, action: PayloadAction<number>) => {
      state.roomIds.push(action.payload);
    },
    removeRoom: (state: SelectedRoomsSliceState, action: PayloadAction<number>) => {
      state.roomIds = state.roomIds.filter((roomId) => roomId !== action.payload)
    },
    cleanSelectedRooms: (state: SelectedRoomsSliceState) => {
      state.roomIds = [];
    }
  }
});

export const { addRoom, removeRoom, cleanSelectedRooms } = selectedRoomsSlice.actions
export const selectSelectedRooms = (state: RootState) => state.selectedRooms.roomIds;
export default selectedRoomsSlice.reducer;
