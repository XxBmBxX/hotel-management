import { Box, Grid, Paper, Icon, Tooltip, Typography, Stack } from '@mui/material';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import DoneIcon from '@mui/icons-material/Done';
import CloseIcon from '@mui/icons-material/Close';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import styles from './roomCardStyles';
import { Room, RoomStatus, RoomType } from "./roomCardTypes";
import { FunctionComponent, useEffect, useState } from "react";
import {
  roomIsCleanToStringMap,
  roomStatusToStringMap,
  roomTypeToIconNameMap
} from "./RoomCardHelpers/enumToStringMaps";
import { useAppDispatch, useAppSelector } from "../../../../Redux/reduxHooks";
import { addRoom, removeRoom, selectSelectedRooms } from "../../../../Reducers/selectedRoomsSlice";

const RoomCard: FunctionComponent<Room> = ({
    id,
    roomNumber,
    roomType,
    roomStatus,
    isClean,
    guestGroup,
    reservedFrom,
    reservedTo
  }) => {

  const [selected, setSelected] = useState<boolean>(false);
  const selectedIds = selectSelectedRooms(useAppSelector((state) => state));
  const dispatch = useAppDispatch();

  const roomIsSelected = (): boolean => {
    return selectedIds.includes(id);
  };

  const setSelectedState = () => {
    if (!roomIsSelected()) {
      dispatch(addRoom(id));
    } else {
      dispatch(removeRoom(id));
    }
  };

  useEffect(() => {
    if (roomIsSelected()) {
      setSelected(true);
    } else {
      setSelected(false);
    }
  }, [selectedIds])

  const getGuestName = (): string => {
    if (guestGroup !== null) {
      return guestGroup.guests[0].firstName + " " + guestGroup.guests[0].lastName;
    } else {
      return "Free Room";
    }
  };

  const formatDate = (date: Date): string => {
    const options: Intl.DateTimeFormatOptions = {
      year: "numeric",
      month: "numeric",
      day: "numeric",
      hour12: false,
      hour: "2-digit",
      minute: "2-digit"
    };

    return new Date(date).toLocaleString("en-GB", options)
  }

  const getReservationPeriod = (): string => {
    if (reservedFrom !== null && reservedTo !== null) {
      return `From ${formatDate(reservedFrom)} to ${formatDate(reservedTo)}`
    } else {
      return "This room is free."
    }
  }

  const getReservationDaysCount = (): number => {
    if (reservedFrom !== null && reservedTo !== null) {
      return new Date(reservedTo).getDate() - new Date(reservedFrom).getDate();
    } else {
      return 0;
    }
  }

  return (
    <Box
      onClick={() => setSelectedState()}
      key={id}
      maxWidth={400}
      padding={1}
    >
      <Paper
        elevation={3}
        sx={selected ? styles.roomCardContainerClicked : styles.roomCardContainer}
      >
        <Grid container>
          <Grid item alignItems={"center"} container xs={12}>
            <Grid item xs={6}>
              <Typography
                color={'white'}
                fontSize={20}
                margin={1}
                align={'left'}
              >
                Room {roomNumber}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography
                component={Stack}
                flexDirection={"row"}
                color={'white'}
                fontSize={20}
                margin={1}
                alignItems={"center"}
                justifyContent={"right"}
              >
                {roomStatusToStringMap.get(RoomStatus[roomStatus])}
                {selected && <CheckBoxIcon sx={styles.selectedIcon}/>}
              </Typography>
            </Grid>
          </Grid>
          <Grid
            padding={2}
            color={'white'}
            justifyContent={'center'}
            alignItems={'center'}
            container
            item
            xs={12}
          >
            <Icon sx={{ fontSize: 56 }}>{roomTypeToIconNameMap.get(RoomType[roomType])}</Icon>
            <Typography
              marginLeft={1}
              fontSize={25}
            >
              {getGuestName()}
            </Typography>
          </Grid>
          <Grid
            padding={1}
            color={'gray'}
            bgcolor={'white'}
            container
            item
            xs={6}
          >
            <Tooltip title={getReservationPeriod()}>
              <CalendarMonthIcon />
            </Tooltip>
            <Typography marginLeft={1}>{getReservationDaysCount()} days</Typography>
          </Grid>
          <Grid
            padding={1}
            color={'gray'}
            bgcolor={'white'}
            justifyContent={'right'}
            container
            item
            xs={6}
          >
            {isClean ? <DoneIcon /> : <CloseIcon />}
            <Typography>{roomIsCleanToStringMap.get(isClean)}</Typography>
          </Grid>
        </Grid>
      </Paper>
    </Box>
  );
};

export default RoomCard;
