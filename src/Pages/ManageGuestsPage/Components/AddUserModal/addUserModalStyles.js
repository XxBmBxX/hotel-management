const styles = {
  modalWrapper: {
    position: 'absolute',
    borderRadius: 3,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    backgroundColor: "white",
    boxShadow: 24,
    p: 4,
  },
  modalButton: {
    marginTop: 1
  },
  closeButton: {
    position: "absolute",
    color: "red",
    top: 10,
    right: 10,
    fontSize: 30,
    transition: "all 0.3s",
    "&:hover": {
      cursor: "pointer",
      color: "darkred"
    }
  },
  marginInput: {
    marginBottom: 1
  }
};

export default styles;
