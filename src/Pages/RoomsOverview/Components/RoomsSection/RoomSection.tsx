import { Box, Grid, Paper, Typography } from "@mui/material";
import { Room } from "../RoomCard/roomCardTypes";
import RoomCard from "../RoomCard/RoomCard";
import { FunctionComponent, useEffect } from "react";

type RoomSectionProps = {
  rooms: Room[];
  roomsType: string;
}

const RoomSection: FunctionComponent<RoomSectionProps> = ({ rooms, roomsType }) => {

  return (
    <Box>
      {
        rooms.length > 0 &&
        <Grid container justifyContent={"center"}>
          <Grid item xs={12}>
            <Paper elevation={2}>
              <Typography
                fontSize={20}
                padding={1}>
                {roomsType}
              </Typography>
            </Paper>
          </Grid>
          <Grid item container>
            {rooms.map((room) => (
              <Grid
                key={room.id}
                container
                item
                justifyContent={"center"}
                xs={12}
                sm={6}
                md={4}
                xl={3}
              >
                <RoomCard
                  id={room.id}
                  roomNumber={room.roomNumber}
                  roomType={room.roomType}
                  roomStatus={room.roomStatus}
                  isClean={room.isClean}
                  guestGroup={room.guestGroup}
                  reservedFrom={room.reservedFrom}
                  reservedTo={room.reservedTo}
                />
              </Grid>
            ))}
          </Grid>
        </Grid>
      }
    </Box>
  );
};

export default RoomSection;
