import { Box, Button, Checkbox, FormControlLabel, FormGroup, FormLabel, Paper, Typography } from "@mui/material";
import styles from "./roomsSideBarStyles";
import { FunctionComponent, useEffect, useState } from "react";
import { cleanSelectedRooms, selectSelectedRooms } from "../../../../Reducers/selectedRoomsSlice";
import { useAppDispatch, useAppSelector } from "../../../../Redux/reduxHooks";
import {
  deleteRoomsRequest,
  markRoomsAsCleanRequest,
  markRoomsAsEmptyRequest
} from "../../../../Axios/axiosRequests";
import AddRoomModal from "../AddRoomModal/AddRoomModal";
import { Room } from "../RoomCard/roomCardTypes";
import MakeReservationModal from "../MakeReservationModal/MakeReservationModal";
import useAuthentication from "../../../Utils/useAuthentication";

type RoomsSideBarProperties = {
  rooms: Room[];
  filterRooms: (filterOptions: Map<string, boolean>) => void;
  updateRooms: Function;
}

const RoomsSideBar: FunctionComponent<RoomsSideBarProperties> = ({ filterRooms, updateRooms, rooms }) => {
  const selectedRoomsState = selectSelectedRooms(useAppSelector((state) => state));
  const { userToken } = useAuthentication();
  const [openAddGuest, setOpenAddGuest] = useState<boolean>(false);
  const [openMakeReservation, setOpenMakeReservation] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  const [filterOptions, setFilterOptions] = useState<Map<string, boolean>>(new Map<string, boolean>([
    ["freeRooms", false],
    ["cleanedRooms", false],
    ["singleRooms", false],
    ["doubleRooms", false],
    ["familyRooms", false],
  ]));

  const handleCheckboxChange = (event: any, key: any) => {
    let newFilterOptions: Map<string, boolean> = new Map<string, boolean>(filterOptions.entries());
    newFilterOptions.set(key, event.target.checked)
    setFilterOptions(newFilterOptions);
  };

  useEffect(() => {
    filterRooms(filterOptions);
  }, [filterOptions]);

  const shouldDisableActions = (): boolean => {
    return selectedRoomsState.length === 0
  }

  return (
    <Box>
      <MakeReservationModal rooms={rooms} fetchData={updateRooms} open={openMakeReservation} setOpen={setOpenMakeReservation} />
      <AddRoomModal rooms={rooms} fetchData={updateRooms} open={openAddGuest} setOpen={setOpenAddGuest} />
      <Paper sx={styles.sideBarContainer} elevation={2}>
        <FormLabel component="legend">Filter by</FormLabel>
        <FormGroup sx={styles.filtersContainer}>
          <FormControlLabel control={<Checkbox onChange={(event => handleCheckboxChange(event, "freeRooms"))} size={"small"} />} label="Free rooms" />
          <FormControlLabel control={<Checkbox onChange={(event => handleCheckboxChange(event, "cleanedRooms"))} size={"small"} />} label="Cleaned rooms" />
          <FormControlLabel control={<Checkbox onChange={(event => handleCheckboxChange(event, "singleRooms"))} size={"small"} />} label="Single rooms" />
          <FormControlLabel control={<Checkbox onChange={(event => handleCheckboxChange(event, "doubleRooms"))} size={"small"} />} label="Double rooms" />
          <FormControlLabel control={<Checkbox onChange={(event => handleCheckboxChange(event, "familyRooms"))} size={"small"} />} label="Family rooms" />
        </FormGroup>
        <Box sx={styles.actionButtonsContainer}>
          <Button
            onClick={() => markRoomsAsEmptyRequest(selectedRoomsState, userToken).then(() => {
              updateRooms();
              dispatch(cleanSelectedRooms());
            })}
            disabled={shouldDisableActions()}
            sx={styles.actionBarButton}
            variant={"contained"}
          >
            <Typography>
              Empty rooms
            </Typography>
          </Button>
          <Button
            onClick={() => markRoomsAsCleanRequest(selectedRoomsState, userToken).then(() => {
              updateRooms();
              dispatch(cleanSelectedRooms());
            })}
            disabled={shouldDisableActions()}
            sx={styles.actionBarButton}
            variant={"contained"}
          >
            <Typography>
              Mark as clean
            </Typography>
          </Button>
          <Button
            disabled={rooms.length === 0}
            onClick={() => setOpenMakeReservation(!openMakeReservation)}
            sx={styles.actionBarButton}
            variant={"contained"}
          >
            <Typography>
              Make reservation
            </Typography>
          </Button>
          <Button
            onClick={() => setOpenAddGuest(!openAddGuest)}
            sx={styles.actionBarButton}
            variant={"contained"}
          >
            <Typography>
              Add room
            </Typography>
          </Button>
          <Button
            onClick={() => deleteRoomsRequest(selectedRoomsState, userToken).then(() => {
              updateRooms();
              dispatch(cleanSelectedRooms());
            })}
            disabled={shouldDisableActions()}
            sx={styles.actionBarButton}
            variant={"contained"}
          >
            <Typography>
              Remove room
            </Typography>
          </Button>
        </Box>
      </Paper>
    </Box>
  );
};

export default RoomsSideBar;
