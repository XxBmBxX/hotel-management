import axios, { AxiosResponse } from "axios";
import { AlertSeverity, Guest, GuestDto, User } from "../GlobalTypes/globalTypes";
import { LoginCredentials, RegisterFormProperties } from "../GlobalTypes/loginRegisterTypes";
import { AppDispatch } from "../Redux/reduxStore";
import { login, logout } from "../Reducers/userSlice";
import { NavigateFunction } from "react-router-dom";
import { ShowAlertFunction } from "../Components/Alert/useAlerts";
import { setIsAuthenticated, setNotAuthenticated } from "../Reducers/isAuthenticatedSlice";
import { MakeReservationDto, Room, RoomDto } from "../Pages/RoomsOverview/Components/RoomCard/roomCardTypes";
import { GridRowId } from "@mui/x-data-grid";

axios.defaults.baseURL = "http://localhost:8080";

const registerRequest = (formProperties: RegisterFormProperties, showAlert: ShowAlertFunction) => {

  axios
    .post("/register", formProperties)
    .then((response) => {
      if (response.data) {
        showAlert("Successful registration!", AlertSeverity.SUCCESS);
      } else {
        showAlert("Username already exists!", AlertSeverity.ERROR);
      }
    })
    .catch(() => {
      showAlert("Something went wrong wit the request!", AlertSeverity.ERROR);
    });
};

const loginRequest = (loginCredentials: LoginCredentials, showAlert: ShowAlertFunction, navigate: NavigateFunction, dispatch: AppDispatch, rememberUser: boolean) => {
  axios
    .post("/log-in", loginCredentials)
    .then((response: AxiosResponse<User>) => {
      if (response.status === 200) {
        dispatch(login(response.data));

        if (rememberUser) {
          localStorage.setItem("user-token", response.headers["user-token"]);
          localStorage.setItem(("userDto"), JSON.stringify(response.data));
        } else {
          sessionStorage.setItem("user-token", response.headers["user-token"]);
          sessionStorage.setItem(("userDto"), JSON.stringify(response.data));
        }

        dispatch(setIsAuthenticated());
        navigate("/rooms");
      }
    })
    .catch(() => {
      showAlert("Wrong password or username!", AlertSeverity.ERROR)
    });
};

const logoutRequest = (navigate: NavigateFunction, dispatch: AppDispatch, user: User | null, userToken: string) => {
  axios
    .post("/log-out", user, { headers: {
      "user-token": userToken
    }})
    .then((response: AxiosResponse<boolean>) => {
      if (response.data) {
        dispatch(logout());
        dispatch(setNotAuthenticated());
        navigate("/")
      }
    });
};

const markRoomsAsCleanRequest = async (roomIds: number[], userToken: string) => {
  return await axios.patch("/mark-rooms-as-clean", roomIds, { headers: {
      "user-token": userToken
  }}).then((response: AxiosResponse<Room[]>) => response);
};

const markRoomsAsEmptyRequest = async (roomIds: number[], userToken: string) => {
  return await axios.patch("/mark-rooms-as-empty", roomIds, { headers: {
      "user-token": userToken
    }});
};

const removeGuestsRequest = async (guestIds: GridRowId[], userToken: string) => {
  return await axios.delete("/guests", {
    data: guestIds,
    headers:{
      "user-token": userToken
    }});
};

const editGuestRequest = async (guest: Guest, userToken: string) => {
  return await axios.patch("/guest", guest, { headers: {
      "user-token": userToken
    }});
};

const addGuestRequest = async (guestDto: GuestDto, userToken:string, showAlert: Function) => {
  return await axios
    .post("/guest", guestDto, { headers: {
      "user-token":  userToken
    }})
    .then((response) => {
      if (response.status === 200) {
        showAlert("Guest was added!", AlertSeverity.SUCCESS);
      }
    })
    .catch(() => {
      showAlert("Such guest already exists.", AlertSeverity.ERROR);
    })
};

const addRoomRequest = async (roomDto: RoomDto, userToken:string, showAlert: Function) => {
  return await axios
    .post("/room", roomDto, { headers: {
        "user-token":  userToken
      }})
    .then((response) => {
      if (response.status === 200) {
        showAlert("Room was added!", AlertSeverity.SUCCESS);
      }
    })
    .catch(() => {
      showAlert("Something went wrong when adding the room.", AlertSeverity.ERROR);
    })
};

const makeReservationRequest = async (makeReservationDto: MakeReservationDto, userToken:string, showAlert: Function) => {
  return await axios
    .post("/make-reservation", makeReservationDto, { headers: {
        "user-token":  userToken
      }})
    .then((response) => {
      if (response.status === 200) {
        showAlert("Reservation was created!", AlertSeverity.SUCCESS);
      }
    })
    .catch(() => {
      showAlert("Something went wrong when creating the reservation.", AlertSeverity.ERROR);
    })
};

const deleteRoomsRequest = async (roomIds: number[], userToken:string) => {
  return await axios
    .delete("/delete-rooms", {
      headers: {
        "user-token":  userToken
      },
      data: roomIds
    })
    .catch((error) => {
      console.log(error)
    })
};

export {
  deleteRoomsRequest,
  makeReservationRequest,
  addRoomRequest,
  addGuestRequest,
  registerRequest,
  loginRequest,
  logoutRequest,
  markRoomsAsCleanRequest,
  markRoomsAsEmptyRequest,
  removeGuestsRequest,
  editGuestRequest
};
