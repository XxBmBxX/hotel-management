import { useAppDispatch, useAppSelector } from "../../Redux/reduxHooks";
import { useEffect, useState } from "react";
import { LoginCredentials } from "../../GlobalTypes/loginRegisterTypes";
import { loginRequest, logoutRequest } from "../../Axios/axiosRequests";
import { NavigateFunction } from "react-router-dom";
import { ShowAlertFunction } from "../../Components/Alert/useAlerts";
import { User } from "../../GlobalTypes/globalTypes";

type UseAuthenticationActions = {
  login: (loginCredentials: LoginCredentials, showAlert: ShowAlertFunction, navigate: NavigateFunction, rememberUser: boolean) => void;
  logout: (navigate: NavigateFunction, user: User | null, userToken: string) => void;
};

type UseAuthentication = {
  authenticationActions: UseAuthenticationActions;
  isAuthenticated: boolean;
  userToken: string;
};

const useAuthentication = (): UseAuthentication => {
  const dispatch = useAppDispatch();
  const authenticationState = useAppSelector((state) => state.authenticationState);

  const authenticationInitialState: UseAuthentication = {
    authenticationActions: {
      login: (loginCredentials, showAlert, navigate, rememberUser) => loginRequest(loginCredentials, showAlert, navigate, dispatch, rememberUser),
      logout: (navigate, user, userToken) => logoutRequest(navigate, dispatch, user, userToken),
    },
    isAuthenticated: authenticationState.isAuthenticated,
    userToken: authenticationState.userToken
  };

  const [authentication, setAuthentication] = useState<UseAuthentication>(authenticationInitialState);

  useEffect(() => {
    setAuthentication((currentState) => ({
      ...currentState,
      isAuthenticated: authenticationState.isAuthenticated,
      userToken: authenticationState.userToken
    }));
  }, [authenticationState]);

  return authentication;
};

export default useAuthentication;
