const styles = {
  loginFormContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "white",
    borderRadius: 2,
    width: "300px",
    boxShadow: 2
  },
  loginPageContentContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "center"
  },
  loginButton: {
    width: "fit-content",
    marginTop: 1
  },
  registrationTextButton: {
    marginTop: 3,
    color: "#F27800"
  }
};

export default styles;
