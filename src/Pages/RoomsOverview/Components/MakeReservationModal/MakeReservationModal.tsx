import React, { FunctionComponent, SetStateAction, useEffect, useState } from "react";
import {
  Autocomplete,
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Modal,
  Select,
  Stack,
  TextField,
  Typography
} from "@mui/material";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { MakeReservationDto, Room, RoomStatus, RoomType } from "../RoomCard/roomCardTypes";
import styles from "../../../ManageGuestsPage/Components/AddUserModal/addUserModalStyles";
import SpringAlert from "../../../../Components/Alert/SpringAlert";
import CloseIcon from "@mui/icons-material/Close";
import useAlerts from "../../../../Components/Alert/useAlerts";
import useAxios from "../../../../Axios/useAxios";
import { AlertSeverity, Guest } from "../../../../GlobalTypes/globalTypes";
import { DateTimePicker } from "@mui/x-date-pickers";
import useAuthentication from "../../../Utils/useAuthentication";
import { roomStatusToStringMap } from "../RoomCard/RoomCardHelpers/enumToStringMaps";
import { makeReservationRequest } from "../../../../Axios/axiosRequests";

type MakeReservationModalProps = {
  rooms: Room[];
  fetchData: Function;
  open: boolean;
  setOpen: React.Dispatch<SetStateAction<boolean>>;
};

const MakeReservationModal: FunctionComponent<MakeReservationModalProps> = ({ open, setOpen, rooms, fetchData }) => {
  const { alertText, isOpen, severity, showAlert } = useAlerts();
  const [reserveFromDate, setReserveFromDate] = useState<Date>(
    new Date(),
  );
  const [reserveToDate, setReserveToDate] = useState<Date>(
    new Date(),
  );
  const { userToken } = useAuthentication();
  const [selectedRoom, setSelectedRoom] = useState<Room>();
  const [reservationRoom, setReservationRoom] = useState<MakeReservationDto>({
    roomNumber: 0,
    reservedFrom: null,
    reservedTo: null,
    reservationPrice: 0,
    guestGroup: {
      guests: []
    }
  });
  const { data } = useAxios<Guest[]>("/guests")

  const handleReserveFromDate = (newValue: Date | null) => {
    if (newValue) {
      setReserveFromDate(newValue);
    }
  };

  const handleReserveToDateChange = (newValue: Date | null) => {
    if (newValue && newValue.getDate() > reserveFromDate.getDate()) {
      setReserveToDate(newValue);
    } else {
      showAlert("Your reserved to date can't be before reserve from date.", AlertSeverity.ERROR)
    }
  };

  useEffect(() => {
    setReservationRoom((prevState) => ({
      ...prevState,
      reservedFrom: reserveFromDate,
      reservedTo: reserveToDate,
      reservationPrice: calculateReservationPrice()
    }));
  }, [reserveFromDate, reserveToDate])

  const checkIfReservationIsValid = (roomType: RoomType, guests: Guest[]): { valid: boolean, reason: string } => {
    switch (roomType) {
      case RoomType.SINGLE:
        if (guests.length > 1) {
          return {valid: false, reason: "This room is single. You can't have more than one guest"};
        }
        break;
      case RoomType.DOUBLE:
        if (guests.length > 2) {
          return {valid: false, reason: "This room is double. You can't have more than two guests"};
        }
        break;
      case RoomType.FAMILY:
        if (guests.length > 3) {
          return {valid: false, reason: "This room is family. You can't have more than three guests"};
        }
        break;
    }

    return {valid: true, reason: ""};
  };

  useEffect(() => {
    const firstRoom: Room = rooms.filter((room) => room.roomStatus === RoomStatus.AVAILABLE).sort((room1, room2) => room1.roomNumber - room2.roomNumber)[0];
    if (firstRoom) {
      setSelectedRoom(firstRoom);
      setReservationRoom((prevState) => ({
        ...prevState,
        roomNumber: Number(firstRoom.roomNumber)
      }));
    }
  }, [rooms]);

  const addGuestsToReservationRoom = (guests: Guest[]) => {
    setReservationRoom((prevState) => ({
      ...prevState,
      guestGroup: {
        guests: guests
      }
    }));
  };

  const getReservationDaysCount = (): number => {
    if (reserveFromDate !== null && reserveToDate !== null) {
      return reserveToDate.getDate() - reserveFromDate.getDate();
    } else {
      return 0;
    }
  };

  const calculateReservationPrice = (): number => {
    const pricePerNight: number = 40;
    let weekDaysCount: number = 0;
    let date1: Date | null = new Date(reserveFromDate);
    let date2: Date | null = new Date(reserveToDate);

    if (date1 !== null && date2 !== null) {
      if (date1.toDateString() === date2.toDateString()) {
        return 0;
      }
      while (date1 <= date2) {
        const day = date1.getDay();

        if (day === 6 || day === 5) {
          weekDaysCount++;
        }
        date1.setDate(date1.getDate() + 1);
      }
    }

    return ((getReservationDaysCount() - weekDaysCount) * pricePerNight) + (weekDaysCount * (pricePerNight - (pricePerNight * 20 / 100)));
  };

  const handleMakeReservationClick = () => {
    if (reservationRoom.guestGroup.guests.length > 0) {
      makeReservationRequest(reservationRoom, userToken, showAlert).then(() => fetchData());
    } else {
      showAlert("Must have at least one guest!", AlertSeverity.ERROR);
    }
  };

  return (
    <Modal
      open={open}
      onClose={() => setOpen(!open)}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={styles.modalWrapper}>
        <Typography align={"center"} marginBottom={2} variant="h4" component="h2">
          Make reservation
        </Typography>
        {selectedRoom && <FormControl fullWidth>
          <InputLabel id="room-type-label">Room type</InputLabel>
          <Select
            labelId="room-type-label"
            id="room-type-select"
            value={selectedRoom.roomNumber}
            onChange={(event) => {
              setSelectedRoom(rooms.find((room) => room.roomNumber === event.target.value));
              setReservationRoom((prevState) => ({
                ...prevState,
                roomNumber: Number(event.target.value)
              }));
            }}
            label="Room type"
          >
            {rooms.filter((room) => room.roomStatus === RoomStatus.AVAILABLE).map((room) => (
              <MenuItem key={room.roomNumber} value={room.roomNumber}>Room {room.roomNumber} - {room.roomType}, {roomStatusToStringMap.get(RoomStatus[room.roomStatus])}</MenuItem>
            ))}
          </Select>
          <Autocomplete
            sx={{
              marginTop: 2,
              marginBottom: 2
            }}
            multiple
            id="tags-standard"
            options={data || []}
            isOptionEqualToValue={(option, value) => option.id === value.id}
            getOptionLabel={(option) => `${option.firstName} ${option.lastName}`}
            onChange={(event, value) => addGuestsToReservationRoom(value)}
            renderInput={(params) => (
              <TextField
                {...params}
                error={!checkIfReservationIsValid(selectedRoom.roomType, reservationRoom.guestGroup.guests).valid}
                helperText={checkIfReservationIsValid(selectedRoom.roomType, reservationRoom.guestGroup.guests).reason}
                variant="outlined"
                label="Guests"
                placeholder="Favorites"
              />
            )}
          />
        </FormControl>}
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Stack marginBottom={1}>
            <DateTimePicker
              label="Reserve from"
              value={reserveFromDate}
              onChange={handleReserveFromDate}
              renderInput={(params) => <TextField {...params} />}
            />
            <Box marginTop={2} />
            <DateTimePicker
              label="Reserve to"
              value={reserveToDate}
              onChange={handleReserveToDateChange}
              renderInput={(params) => <TextField {...params} />}
            />
          </Stack>
        </LocalizationProvider>
        <Typography>Price: {calculateReservationPrice()}$</Typography>
        <SpringAlert severity={severity} alertText={alertText} isOpen={isOpen} />
        {selectedRoom && <Button
          onClick={handleMakeReservationClick}
          disabled={!checkIfReservationIsValid(selectedRoom.roomType, reservationRoom.guestGroup.guests).valid}
          sx={styles.modalButton}
          variant="contained">
          Make reservation
        </Button>}
        <CloseIcon onClick={() => setOpen(!open)} sx={styles.closeButton} />
      </Box>
    </Modal>
  );
}

export default MakeReservationModal;
