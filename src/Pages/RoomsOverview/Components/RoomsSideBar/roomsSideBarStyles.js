const styles = {
  sideBarContainer: {
    padding: "10px 20px",
    backgroundColor: "white",
    marginBottom: "15px",
    minWidth: "fit-content",
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  filtersContainer: {
    marginLeft: "10px",
    display: "flex",
    flexDirection: "row"
  },
  actionBarButton: {
    margin: 1
  },
  actionButtonsContainer: {
    marginLeft: "auto"
  }
};

export default styles;
