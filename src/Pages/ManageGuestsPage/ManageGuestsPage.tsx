import { DataGrid, GridRowId } from '@mui/x-data-grid';
import useAxios from "../../Axios/useAxios";
import { AlertSeverity, Guest } from "../../GlobalTypes/globalTypes";
import { useEffect, useState } from "react";
import { Box, Button, Paper, Stack, Typography } from "@mui/material";
import styles from "./manageGuestsPageStyles"
import columns from "./tableColumns";
import DeleteIcon from '@mui/icons-material/Delete';
import AddBoxIcon from '@mui/icons-material/AddBox';
import { editGuestRequest, removeGuestsRequest } from "../../Axios/axiosRequests";
import { useAppSelector } from "../../Redux/reduxHooks";
import SpringAlert from "../../Components/Alert/SpringAlert";
import useAlerts from "../../Components/Alert/useAlerts";
import AddUserModal from "./Components/AddUserModal/AddUserModal";

const ManageGuestsPage = () => {
  const [selectedIds, setSelectedIds] = useState<GridRowId[]>([]);
  const authenticationState = useAppSelector((state) => state.authenticationState);
  const { data, fetchData } = useAxios<Guest[]>("/guests");
  const { alertText, isOpen, severity, showAlert } = useAlerts();
  const [open, setOpen] = useState<boolean>(false);
  const [guests, setGuests] = useState<Guest[]>([]);
  const [pageSize, setPageSize] = useState<number>(5);

  useEffect(() => {
    if (data) {
      setGuests(data);
    }
  }, [data]);

  const getEditedGuest = (rowId: string | number, field: string, value: string): Guest => {
    let editedGuest: Guest | undefined = guests.find((guest) => guest.id = rowId);
    if (editedGuest) {
      editedGuest[field as keyof Guest] = value;
      return editedGuest
    }

    return guests[0];
  };

  const handleRemoveGuestClick = () => {
    if (selectedIds.length > 0) {
      removeGuestsRequest(selectedIds, authenticationState.userToken).then(() => {
        fetchData();
        showAlert("Guests successfully removed!", AlertSeverity.SUCCESS);
      });
    } else {
      showAlert("No guests selected!", AlertSeverity.ERROR);
    }
  };

  return (
    <Box sx={styles.tableContainer}>
      <AddUserModal setOpen={setOpen} open={open} fetchData={fetchData} />
      <Paper>
        <Box sx={styles.headerContainer}>
          <Typography padding={1} fontSize={25} align={"center"}>
            Guests list
          </Typography>
          <Box sx={styles.headerButtonsContainer}>
            <Button
              onClick={handleRemoveGuestClick}
              sx={styles.headerButton}
              variant={"contained"}
            >
              <Typography
                component={Stack}
                flexDirection={"row"}
                alignItems={"center"}>
                <DeleteIcon />
                <Typography fontSize={15} marginLeft={1}>
                  {selectedIds.length > 1 ? "Remove guests" : "Remove guest"}
                </Typography>
              </Typography>
            </Button>
            <Button
              onClick={() => setOpen(!open)}
              sx={styles.headerButton}
              variant={"contained"}
            >
              <Typography
                component={Stack}
                flexDirection={"row"}
                alignItems={"center"}>
                <AddBoxIcon />
                <Typography fontSize={15} marginLeft={1}>
                  Add guest
                </Typography>
              </Typography>
            </Button>
          </Box>
        </Box>
        <Box padding={1}>
          <SpringAlert severity={severity} alertText={alertText} isOpen={isOpen} />
          <DataGrid
            rows={guests}
            columns={columns}
            pageSize={pageSize}
            onPageSizeChange={(pageSize) => setPageSize(pageSize)}
            onCellEditCommit={(params) => editGuestRequest(
              getEditedGuest(params.id, params.field, params.value), authenticationState.userToken
            )
              .then(() => showAlert("Guest was edited!", AlertSeverity.SUCCESS))}
            rowsPerPageOptions={[5, 10, 15, 20]}
            checkboxSelection
            onSelectionModelChange={(selectionModel) => setSelectedIds(selectionModel)}
            disableSelectionOnClick
            autoHeight
          />
        </Box>
      </Paper>
    </Box>
  );
};
export default ManageGuestsPage;
