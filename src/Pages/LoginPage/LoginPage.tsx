import { Box, Button, Checkbox, FormControlLabel, TextField, Typography } from "@mui/material";
import styles from  "./loginPageStyles"
import React, { useEffect, useState } from "react";
import { LoginCredentials } from "../../GlobalTypes/loginRegisterTypes";
import { validateTextField } from "../Utils/LoginRegisterHelpers/validateInputFields";
import useAlerts from "../../Components/Alert/useAlerts";
import SpringAlert from "../../Components/Alert/SpringAlert";
import useAuthentication from "../Utils/useAuthentication";
import { useNavigate } from "react-router-dom";

const LoginPage = () => {
  const { isAuthenticated, authenticationActions: { login } } = useAuthentication();
  const navigate = useNavigate();

  useEffect(() => {
    if (isAuthenticated) {
      navigate("/rooms");
    }
  }, [])

  const initialLoginCredentials: LoginCredentials = {
    password: "",
    username: ""
  }
  const [loginCredentials, setLoginCredentials] = useState<LoginCredentials>(initialLoginCredentials);
  const [rememberUser, setRememberUser] = useState<boolean>(false);
  const { alertText, isOpen, severity, showAlert } = useAlerts();

  const getFieldValue = (inputId: string): string => {
    return loginCredentials[inputId as keyof LoginCredentials];
  };

  const checkAllFields = (): boolean => {
    return Object.keys(loginCredentials).some((property) => !validateTextField(getFieldValue(property)).isValid);
  };

  const handleInput = (value: string, inputId: string) => {
    setLoginCredentials((currentState) => ({
      ...currentState,
      [inputId]: value
    }))
  };

  return (
    <Box sx={styles.loginPageContentContainer}>
      <Box padding={6} paddingTop={2} paddingBottom={2} sx={styles.loginFormContainer}>
        <Typography marginBottom={2} color={"gray"} fontSize={18} align={"center"}>
          Make your hotelling easier!
        </Typography>
        <Typography fontSize={25} align={"center"}>
          Log in
        </Typography>
        <TextField
          required
          fullWidth
          error={!validateTextField(getFieldValue("username")).isValid}
          helperText={validateTextField(getFieldValue("username")).helperText}
          value={loginCredentials.username}
          onChange={(event) => handleInput(event.target.value, "username")}
          margin={"dense"}
          label="Username"
          variant="filled"
        />
        <TextField
          required
          fullWidth
          error={!validateTextField(getFieldValue("password")).isValid}
          helperText={validateTextField(getFieldValue("password")).helperText}
          value={loginCredentials.password}
          onChange={(event) => handleInput(event.target.value, "password")}
          margin={"dense"}
          label="Password"
          variant="filled"
          type={"password"}
        />
        <FormControlLabel control={<Checkbox checked={rememberUser} onChange={() => setRememberUser(!rememberUser)} />} label="Remember me" />
        <SpringAlert severity={severity} alertText={alertText} isOpen={isOpen} />
        <Button
          disabled={isOpen || checkAllFields()}
          onClick={() => login(loginCredentials, showAlert, navigate, rememberUser)}
          sx={styles.loginButton}
          variant="contained">
          Login
        </Button>
        <Button
          onClick={() => navigate("/register")}
          sx={styles.registrationTextButton}
          variant="text">
          Don't have an account?
        </Button>
      </Box>
    </Box>
  );
};

export default LoginPage;
