import {configureStore} from '@reduxjs/toolkit';
import userSlice from "../Reducers/userSlice";
import useAuthenticationSlice from "../Reducers/isAuthenticatedSlice";
import selectedRoomsSlice from "../Reducers/selectedRoomsSlice";

export const reduxStore = configureStore({
  reducer: {
    userState: userSlice,
    authenticationState: useAuthenticationSlice,
    selectedRooms: selectedRoomsSlice,
  },
});

export type RootState = ReturnType<typeof reduxStore.getState>;
export type AppDispatch = typeof reduxStore.dispatch;
