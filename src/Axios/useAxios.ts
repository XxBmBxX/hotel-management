import axios, { AxiosError } from "axios";
import { useCallback, useEffect, useState } from "react";
import useAuthentication from "../Pages/Utils/useAuthentication";

type ReturnedData<T> = {
  data: T | null;
  error: AxiosError | null;
  loading: boolean;
  fetchData: Function;
};

const useAxios = <T>(endpoint: string): ReturnedData<T> => {
  const initialState: ReturnedData<T> = {
    data: null,
    error: null,
    loading: false,
    fetchData: () => {}
  };
  const [axiosResponse, setAxiosResponse] = useState<ReturnedData<T>>(initialState);
  const { userToken } = useAuthentication();

  const fetchData = useCallback(() => {
    setAxiosResponse({
      data: null,
      error: null,
      loading: true,
      fetchData: fetchData
    });
    axios.get<T>(endpoint, {
      headers: {
        "user-token": userToken
      }
    })
      .then((response) => {
        setAxiosResponse({
          data: response.data,
          error: null,
          loading: false,
          fetchData: fetchData
        });
      })
      .catch((error) => {
        setAxiosResponse({
          data: null,
          error: error,
          loading: false,
          fetchData: fetchData
        });
      });
  }, [endpoint])

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return axiosResponse;
}

export default useAxios;
