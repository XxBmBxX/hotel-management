import { Box, Button, Stack, Typography } from "@mui/material";
import BedroomParentIcon from '@mui/icons-material/BedroomParent';
import LogoutIcon from '@mui/icons-material/Logout';
import LoginIcon from '@mui/icons-material/Login';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import styles from "./navigationMenuStyles"
import { useLocation, useNavigate } from "react-router-dom";
import useAuthentication from "../../Pages/Utils/useAuthentication";
import { useAppSelector } from "../../Redux/reduxHooks";
import { selectUser } from "../../Reducers/userSlice";

const NavigationMenu = () => {
  const { isAuthenticated, userToken, authenticationActions: { logout } } = useAuthentication();
  const userState = selectUser(useAppSelector((state) => state));
  const currentLocation = useLocation();
  const navigate = useNavigate();

  const isActiveLink = (pathname: string): boolean => {
    return currentLocation.pathname === pathname;
  };

  return (
    <Box padding={1} sx={styles.navigationMenuContainer}>
      {isAuthenticated && <Button
        onClick={() => navigate("/rooms")}
        sx={isActiveLink("/rooms") ? styles.navigationButtonActive : styles.navigationButton}
        variant={"text"}
      >
        <Typography
          component={Stack}
          flexDirection={"row"}
          alignItems={"center"}>
          <BedroomParentIcon/>
          <Typography fontSize={15} marginLeft={1}>
            Manage rooms
          </Typography>
        </Typography>
      </Button>}
      {isAuthenticated && <Button
        onClick={() => navigate("/guests")}
        sx={isActiveLink("/guests") ? styles.navigationButtonActive : styles.navigationButton}
        variant={"text"}
      >
        <Typography
          component={Stack}
          flexDirection={"row"}
          alignItems={"center"}>
          <BedroomParentIcon/>
          <Typography fontSize={15} marginLeft={1}>
            Manage guests
          </Typography>
        </Typography>
      </Button>}
      <Box sx={styles.authenticationContainer}>
        {isAuthenticated && <Typography marginRight={1}>
          {userState && `${userState.firstName} ${userState.lastName}`}
        </Typography>}
        {!isAuthenticated &&
        <Button
          onClick={() => navigate("/login")}
          sx={styles.authenticationButton}
          variant={"text"}
        >
          <Typography
            component={Stack}
            flexDirection={"row"}
            alignItems={"center"}>
            <LoginIcon/>
            <Typography marginLeft={1}>
              Login
            </Typography>
          </Typography>
        </Button>}
        {!isAuthenticated &&
        <Button
          onClick={() => navigate("/register")}
          sx={styles.authenticationButton}
          variant={"text"}
        >
          <Typography
            component={Stack}
            flexDirection={"row"}
            alignItems={"center"}>
            <AppRegistrationIcon/>
            <Typography marginLeft={1}>
              Register
            </Typography>
          </Typography>
        </Button>}
        {isAuthenticated &&
        <Button
          onClick={() => logout(navigate, userState, userToken)}
          sx={styles.authenticationButton}
          variant={"text"}>
          <Typography
            component={Stack}
            flexDirection={"row"}
            alignItems={"center"}>
            <LogoutIcon/>
            <Typography marginLeft={1}>
              Log out
            </Typography>
          </Typography>
        </Button>}
      </Box>
    </Box>
  );
};

export default NavigationMenu;
