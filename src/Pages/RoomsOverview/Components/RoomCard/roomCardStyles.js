const styles = {
  roomCardContainer: {
    userSelect: "none",
    backgroundColor: "#5099F4",
    borderRadius: "10px",
    overflow: "hidden",
    transition: "all 0.3s",
    "&:hover": {
      cursor: "pointer",
      backgroundColor: "#3E76BD"
    }
  },
  roomCardContainerClicked: {
    backgroundColor: "#3E76BD",
    borderRadius: "10px",
    overflow: "hidden",
    transition: "all 0.3s",
    "&:hover": {
      cursor: "pointer"
    }
  },
  selectedIcon: {
    marginLeft: 1
  }
};

export default styles;
