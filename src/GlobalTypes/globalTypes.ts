import { Room } from "../Pages/RoomsOverview/Components/RoomCard/roomCardTypes";

enum AlertSeverity {
  SUCCESS,
  ERROR,
  WARNING
}

type Note = {
  noteId: number;
  note: string;
};

type NoteGroup = {
  noteGroupId: number;
  notes: Note[];
};

type Guest = {
  id: string | number;
  firstName: string;
  lastName: string;
  phoneNumber: string;
};

type GuestDto = {
  firstName: string;
  lastName: string;
  phoneNumber: string;
};

type GuestGroup = {
  guests: Guest[];
};

type AlertProperties = {
  severity: AlertSeverity;
  alertText: string;
  isOpen: boolean;
};

type User = {
  username: string;
  firstName: string;
  lastName: string;
};

type ProtectedRouteProperties = {
  component: JSX.Element;
};

type RoomsOverview = {
  rooms: Room[];
  filteredRooms: Room[];
}

export type { NoteGroup, GuestGroup, Guest, Note, AlertProperties, User, ProtectedRouteProperties, RoomsOverview, GuestDto };
export { AlertSeverity };
