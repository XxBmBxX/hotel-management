import { ShowAlertFunction } from "../Components/Alert/useAlerts";
import { NavigateFunction } from "react-router-dom";
import { AppDispatch } from "../Redux/reduxStore";

type RegisterFormProperties = {
  firstName: string;
  lastName: string;
  username: string;
  password: string;
};

type LoginCredentials = {
  username: string;
  password: string;
};

type InputHelperText = {
  inputId: string;
  helperText: string;
};

type TextFieldConstraints = {
  minCharacters: number;
  maxCharacters: number;
};

type TextFieldValidity = {
  isValid: boolean;
  helperText: string;
};

type LoginFunction = {
  (parameters: LoginFunctionProperties): void;
};

type LoginFunctionProperties = {
  loginCredentials: LoginCredentials;
  showAlert: ShowAlertFunction;
  navigate: NavigateFunction;
  dispatch: AppDispatch;
};

type LogoutFunction = {
  (parameters: LogoutFunctionProperties): void;
};

type LogoutFunctionProperties = {
  navigate: NavigateFunction;
  dispatch: AppDispatch;
};

export type { RegisterFormProperties, InputHelperText, TextFieldConstraints, TextFieldValidity, LoginCredentials, LogoutFunction, LoginFunction };
