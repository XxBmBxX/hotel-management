import React from 'react';
import './App.css';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import NavigationMenu from "./Components/Navigation/NavigationMenu";
import RoomsOverviewPage from "./Pages/RoomsOverview/RoomsOverviewPage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import ProtectedRoute from "./Components/Navigation/ProtectedRoute";
import EnhancedTable from "./Pages/ManageGuestsPage/ManageGuestsPage";
import ManageGuestsPage from "./Pages/ManageGuestsPage/ManageGuestsPage";

function App() {

  return (
    <BrowserRouter>
      <div>
        <NavigationMenu />
        <div style={{
          padding: "10px",
        }}>
          <Routes>
            <Route path={"/"} element={<LoginPage />} />
            <Route path={"/register"} element={<RegisterPage />} />
            <Route path={"/login"} element={<LoginPage />} />
            <Route
              path={"/rooms"}
              element={
                <ProtectedRoute component={<RoomsOverviewPage />} />
              }
            />
            <Route
              path={"/guests"}
              element={
                <ProtectedRoute component={<ManageGuestsPage />} />
              }
            />
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
