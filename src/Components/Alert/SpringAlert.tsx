import { Alert, AlertColor, Collapse } from "@mui/material";
import { FunctionComponent } from "react";
import { AlertProperties, AlertSeverity } from "../../GlobalTypes/globalTypes";

const SpringAlert: FunctionComponent<AlertProperties> = ({alertText, isOpen, severity}) => {
  const getSeverity = (): AlertColor => {
    switch (severity) {
      case AlertSeverity.ERROR: return "error";
      case AlertSeverity.SUCCESS: return "success";
      case AlertSeverity.WARNING: return "warning";
      default: return "error";
    }
  };

  return (
    <Collapse in={isOpen}>
      <Alert severity={getSeverity()}>{alertText}</Alert>
    </Collapse>
  );
};

export default SpringAlert;
