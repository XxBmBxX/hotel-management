import { Box, Button, FormControl, InputLabel, MenuItem, Modal, Select, TextField, Typography } from "@mui/material";
import styles from "../../../ManageGuestsPage/Components/AddUserModal/addUserModalStyles";
import SpringAlert from "../../../../Components/Alert/SpringAlert";
import { addRoomRequest } from "../../../../Axios/axiosRequests";
import CloseIcon from "@mui/icons-material/Close";
import React, { FunctionComponent, SetStateAction, useState } from "react";
import { Room, RoomDto, RoomStatus, RoomType } from "../RoomCard/roomCardTypes";
import useAuthentication from "../../../Utils/useAuthentication";
import useAlerts from "../../../../Components/Alert/useAlerts";

type AddRoomModalProps = {
  rooms: Room[];
  fetchData: Function;
  open: boolean;
  setOpen: React.Dispatch<SetStateAction<boolean>>;
};

const AddRoomModal: FunctionComponent<AddRoomModalProps> = ({ fetchData, open, setOpen, rooms }) => {

  const roomInitialState: RoomDto = {
    isClean: false,
    roomNumber: 0,
    roomStatus: RoomStatus.AVAILABLE,
    roomType: RoomType.SINGLE
  };
  const [roomState, setRoomState] = useState<RoomDto>(roomInitialState);
  const { userToken } = useAuthentication();
  const { alertText, isOpen, severity, showAlert } = useAlerts();

  const checkIfRoomExists = (): boolean => {
    return rooms.some((room) => room.roomNumber === roomState.roomNumber)
  };

  return (
    <Modal
      open={open}
      onClose={() => setOpen(!open)}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={styles.modalWrapper}>
        <Typography align={"center"} variant="h4" component="h2">
          Add room
        </Typography>
        <TextField
          required
          fullWidth
          error={checkIfRoomExists()}
          helperText={checkIfRoomExists() ? "Such room already exists" : ""}
          value={roomState.roomNumber}
          onChange={(event) => setRoomState((prevState) => ({
            ...prevState,
            roomNumber: Number(event.target.value)
          }))}
          margin={"dense"}
          label="Room number"
          variant="filled"
          type={"number"}
        />
        <FormControl sx={{
          marginTop: 1,
          marginBottom: 1
        }} fullWidth>
          <InputLabel id="room-type-label">Room type</InputLabel>
          <Select
            labelId="room-type-label"
            id="room-type-select"
            value={roomState.roomType}
            label="Room type"
            onChange={(event) => setRoomState((prevState) => ({
              ...prevState,
              roomType: event.target.value as RoomType
            }))}
          >
            <MenuItem value={RoomType.SINGLE}>Single</MenuItem>
            <MenuItem value={RoomType.DOUBLE}>Double</MenuItem>
            <MenuItem value={RoomType.FAMILY}>Family</MenuItem>
          </Select>
        </FormControl>
        <SpringAlert severity={severity} alertText={alertText} isOpen={isOpen} />
        <Button
          disabled={checkIfRoomExists()}
          sx={styles.modalButton}
          onClick={() => addRoomRequest(roomState, userToken, showAlert).then(() => fetchData())}
          variant="contained">
          Add room
        </Button>
        <CloseIcon onClick={() => setOpen(!open)} sx={styles.closeButton} />
      </Box>
    </Modal>
  );
};

export default AddRoomModal
