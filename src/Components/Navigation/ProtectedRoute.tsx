import { Navigate } from "react-router-dom";
import { FunctionComponent } from "react";
import { ProtectedRouteProperties } from "../../GlobalTypes/globalTypes";
import useAuthentication from "../../Pages/Utils/useAuthentication";

const ProtectedRoute: FunctionComponent<ProtectedRouteProperties> = ({ component }) => {
  const { isAuthenticated } = useAuthentication();

  if (!isAuthenticated) {
    return <Navigate to="/login" replace />;
  }

  return component;
};

export default ProtectedRoute;
